
# Lost in Translation

Build an online sign language translator as a Single Page Application using the React framework. 

## Structure

_Shows only the most important files._

<pre>
├───public
│   ├───individial_signs
│   └───index.html
│───src
│   │───components
│   │   ├───Login.jsx
│   │   ├───NavBar.jsx
│   │   ├───Profile.jsx
│   │   └───Translator.jsx
│   │───hoc
│   │   └───UserContext.jsx
│   └───index.js
│───.gitignore
│───Component Tree.png
│───package.json
└───README.md
</pre>

## Usage

This application is deployed in [Heroku](https://gs-lost-in-translation.herokuapp.com/).

### Installation

1. Install <code>node</code> and <code>git</code>.
2. Clone repo over _https_ using <code>git clone https://gitlab.com/Glaysa/lost-in-translation.git</code>.

### Running

1. Open a terminal and run the following commands:
    - <code>npm install</code> to install the necessary packages.
    - <code>npm start</code> to start the app in development mode.
3. Open http://localhost:3000 to view it in your browser.

## Contributors
* Glaysa Fernandez
* [Sindre Thomassen](https://gitlab.com/sindre.thomassen)
