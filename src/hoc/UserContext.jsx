import { createContext, useState } from "react";

export const UserContext = createContext(null);

const UserProvider = ({ children }) => {
  
  const userInStorage = JSON.parse(localStorage.getItem("user"));
  const [user, setUser] = useState(userInStorage);

  return (
    <UserContext.Provider value={[user, setUser]}>
      { children }
    </UserContext.Provider>
  );
}

export default UserProvider;