import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import UserProvider from './hoc/UserContext';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Login from './components/Login';
import Translator from './components/Translator';
import Profile from './components/Profile';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <BrowserRouter>
          <UserProvider>
            <App>
                <Routes>
                    <Route path={'/login'} element={ <Login /> } />
                    <Route path={'/translate'} element={<Translator/>}/>
                    <Route path={'/profile'} element={<Profile />}/>
                    <Route path={'/*'} element={ <Navigate to={'/login'} /> } />
                </Routes>
            </App>
          </UserProvider>
      </BrowserRouter>
  </React.StrictMode>
);
