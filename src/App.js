import './App.css';
import NavBar from './components/NavBar';
import React from 'react';

function App(props) {
  return (
    <React.Fragment>
        <div className="container-fluid bg-theme">
            <NavBar />
        </div>
      { props.children }
      </React.Fragment>
  );
}

export default App;
