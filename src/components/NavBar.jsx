import { NavLink, Navigate } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from '../hoc/UserContext';

function NavBar() {
    
    const [ user, setUser ] = useContext(UserContext);
    const logout = () => {
        setUser(null);
        localStorage.removeItem('user');
        return (
            <Navigate to="/login" />
        );
    }

    return (
        <header className="container-fluid border-bottom border-theme">
            <nav className="navbar">
                <div className="container">
                    <NavLink className="navbar-brand" to="/">
                        <h2>Lost in Translation</h2>
                    </NavLink>
                    
                    {   /* Renders username if user is logged in */
                        user != null && 
                        <span className="navbar-text d-flex align-items-center">
                            <NavLink to={'/profile'}><span className="navbar-text">
                                {user.username} <i className="ms-2 fs-4 bi bi-person-circle"></i></span>
                            </NavLink>
                            {
                                user != null &&
                                <button className="ms-3 btn btn-danger" onClick={ logout }>Logout</button>
                            }
                        </span> 
                    }
                </div>
            </nav> 
        </header>
    );
}

export default NavBar;