import React, { useContext, useEffect } from "react";
import { useState } from "react";
import {UserContext} from "../hoc/UserContext";
import {Navigate} from "react-router-dom";

export default function Translator() {
    const API_URL = process.env.REACT_APP_API_URL;
    const API_KEY = process.env.REACT_APP_API_KEY;

    const [letters, setLetters] = useState([]);
    const alphabeth = "abcdefghijklmnopqrstuvwxyz";
    const [user, setUser] = useContext(UserContext);

    // Runs when user updates. Pushes the updated user object to the database
    useEffect(() => {
        if(user == null)
            return;
        fetch(`${API_URL}/${user.id}`, {
            method: 'PATCH',
            headers: {
                'X-API-Key': API_KEY,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                translations: user.translations,
                offset: user.offset
            })
        }).catch(error => alert(error));
    }, [user])

    // Translates the input text into sign language image sequence
    const translateTextToSignanguage = () => {
        const textElement = document.getElementById("original-text");

        let originalText = textElement.value.toLowerCase();
        let text = originalText.split('');
        text = text.filter(letter => alphabeth.includes(letter));
        setLetters(text);
        if(originalText.length > 0)
            pushNewTranslation(originalText)
    }

    // Pushes new translation to user and updates user state
    const pushNewTranslation = (text) => {
        const updatedUser = JSON.parse(JSON.stringify(user));

        updatedUser.translations.push(text);
        updatedUser.offset = Math.max(updatedUser.offset, updatedUser.translations.length - 10);
        setUser(updatedUser);
        localStorage.setItem('user', JSON.stringify(updatedUser));
    }

    // Redirects user to login page if not logged in
    if(user == null) {
        return (
            <Navigate to="/login" />
        );
    }

    return (
        <React.Fragment>
            <section className="container-fluid bg-theme">
                <div className="container position-relative">
                    <div className="row py-5">
                        <div className="bg-light p-4 rounded-4 w-100 mt-4 shadow purple-border">
                            <div className="input-group d-flex align-items-center">
                                <i className="bi bi-keyboard fs-1 me-3"></i>
                                <input id="original-text" type="text" maxLength={24}
                                       className="form-control fs-3 rounded-4"
                                       placeholder="What would you like to translate?"
                                       onKeyUp={e => { if(e.key == "Enter") translateTextToSignanguage();}}/>
                                <button type="button" className="btn fs-1" onClick={ translateTextToSignanguage }>
                                    <i className="bi bi-translate text-theme"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div id="translation-results" className="p-4 rounded-4 container mt-4 shadow">
                { letters.map((letter, i) => {
                    return (
                        <img src={`${process.env.PUBLIC_URL}/individial_signs/${letter}.png`} alt={letter} key={i} />
                    );
                }) }
            </div>
        </React.Fragment>
    );
}