import { useForm } from 'react-hook-form';
import { useContext, useState } from 'react';
import { UserContext } from '../hoc/UserContext';
import { Navigate } from 'react-router';

const API_URL = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;
const usernameConfig = { required: true, maxLength: 16, minLength: 3 }

function Login() {
    
    const [ user, setUser] = useContext(UserContext);
    const [ loading, setLoading ] = useState(false);
    const { register, handleSubmit, formState: { errors } } = useForm();
    
    // Redirects user to translate page if user is logged in
    if(user != null) return <Navigate to={"/translate"} />

    // Logs in the user
    const onSubmit = async ({ username }) => {
        setLoading(true);
        let userExist = await userAlreadyExists(username);
        userExist ? getUser(username) : createUser(username);
        setLoading(false);
    }

    // checks if user already exists
    const userAlreadyExists = async (username) => {
        const response = await fetch(`${API_URL}?username=${username}`);
        const data = await response.json();
        return data.length > 0;
    }

    // creates and stores user in the db
    const createUser = async (username) => {
        const newUser = { username: username, translations: [], offset: 0 }
        const userFromDb = await postUser(newUser);
        localStorage.setItem("user", JSON.stringify(userFromDb));
        setUser(userFromDb);
    }

    // GET: gets user from db
    const getUser = async (username) => {
        const response = await fetch(`${API_URL}?username=${username}`);
        const data = await response.json();
        localStorage.setItem("user", JSON.stringify(data[0]));
        setUser(data[0]);
    }

    // POST: Stores user in the db
    const postUser = async (user) => {
        const response = await fetch(API_URL, {
            method: 'POST',
            headers: {
                'X-API-Key': API_KEY,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }).catch(error => alert(error));
        return await response.json();
    }

    // Checks for input errors
    const inputErrors = (() => {
        if(!errors.username && !errors.apiError) return null;
        if(errors?.username?.type === "required") return "Please enter a valid username.";
        if(errors?.username?.type === "minLength") return "Username must be at least 3 characters.";
        if(errors?.username?.type === "maxLength") return "Username can have up to 16 characters.";
    })();

    return (
        <section className="container-fluid bg-theme">
            <div className="container position-relative">

                {/* Introduction */}
                <div className="row py-5">
                    <div className="col-lg-3 d-flex justify-content-center">
                        <img id="logo-img" src="/Logo.png" alt="logo" className="img-fluid"/>
                    </div>
                    <div className="col-lg-9 d-flex flex-column justify-content-center">
                        <h1 className="text-center text-lg-start">Lost in Translation</h1>
                        <h3 className="text-center text-lg-start">Get Started</h3>
                    </div>
                </div>

                {/* Username input */}
                <div id="login-input-wrapper" className="bg-light p-4 rounded-4 w-100 mt-4 shadow position-absolute top-100 start-50 translate-middle">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="input-group d-flex align-items-center">
                                <i className="bi bi-keyboard fs-1 me-3"></i>
                                <input type="text" 
                                    className="form-control fs-3 rounded-4" 
                                    placeholder="What's your username?"
                                    disabled={loading}
                                    {...register("username", usernameConfig)} />
                                <button type="submit" className="btn border-0 fs-1" disabled={loading}>
                                    <i className="bi bi-arrow-right-circle-fill text-theme"></i>
                                </button>
                        </div>
                    </form>

                    {   /* Show loading indicator when logging the user in. */
                        loading &&
                        <div className="d-flex flex-column align-items-center p-3">
                            <span className='spinner-border'></span>
                            <span className="text-muted mt-2">Logging in...</span>
                        </div>
                    }

                    {   /* Show input errors. */
                        <span className={`text-danger text-center mt-3 fs-6 ${inputErrors === null  ? "d-none" : "d-block"}`}>{inputErrors}</span>
                    }
                </div>
            </div>
        </section>
    );
}

export default Login;