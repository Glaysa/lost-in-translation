import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { UserContext } from "../hoc/UserContext";

const API_URL = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;

function Profile() {

  const [ user, setUser ] = useContext(UserContext);
  const [ translations, setTranslations ] = useState(user?.translations);
  const [ loading, setLoading ] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    // Redirects user to login page if they try to access profile page without logging in
    if(user === null) return navigate("/login");

    // Only shows the first 10 translations of the user
    setTranslations(user?.translations?.slice(user.offset, user.offset + 10));
  }, [user, navigate]);

  // Clears history
  const clearHistory = async () => {
    setLoading(true);
    await updateOffset();
    setLoading(false);
  }

  // PATCH: Updates offset instead of deleting translations
  const updateOffset = async () => {
    const response = await fetch(`${API_URL}/${user.id}`, {
      method: 'PATCH',
      headers: {
          'X-API-Key': API_KEY,
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        offset: user?.translations?.length
      })
    });
    const data = await response.json();
    getTranslations(data);
  }

  // Modifies user translations list based on an offset and a limit
  const getTranslations = (user) => {
    const modifiedList = user.translations.slice(user.offset, user.offset + 10);
    setTranslations(modifiedList);
    setUser(user);
  }

  return (
    <section className="container-fluid bg-theme py-5">
      <div className="container">

        {/* Page Intro */}
        <div className="row d-flex justify-content-center">
          <div className="col-lg-9 text-center d-flex flex-column justify-content-center">
            <h1>Profile</h1>
            <h3>Translation History</h3>
          </div>
        </div>

        {/* Translation History */}
        <div id="login-input-wrapper" className="bg-light p-4 rounded-4 w-100 mt-4 shadow">
            <ul className="list-group list-group-numbered">
              {
                (user != null && translations?.length > 0)
                ? translations.map((translation, index) => {
                    return <li key={index} className="list-group-item">{translation}</li>
                  }) 
                : <span className="d-block text-center text-muted mb-3">History is empty.</span>
              }
            </ul>

            {/* Clear History and logout button */}
            <div className="d-flex justify-content-center mt-3">
              <button className="btn btn-sm btn-outline-secondary"onClick={clearHistory}>Clear History</button>
            </div>

            { /* Show loading indicator when clearing the history */
              loading &&
              <div className="d-flex flex-column align-items-center mt-3">
                <span className='spinner-border'></span>
                <span className="text-muted mt-2">Clearing history...</span>
              </div>
            }
        </div>
      </div>
    </section>
  );
}

export default Profile;